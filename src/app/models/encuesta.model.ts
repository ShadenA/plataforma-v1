export class Encuesta {
  constructor(
    public titulo?: string,
    public fecha?: Date,
    public estado?: string,
    public id_Encuesta?: number,
  ) { }
}
