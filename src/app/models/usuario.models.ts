export class Usuario {
  constructor(
    public nombre: string,
    public correo: string,
    public password: string,
    public id?: string,
    public img?: string,
    public google?: boolean,
    public conexion?: Date
  ) { }
}
