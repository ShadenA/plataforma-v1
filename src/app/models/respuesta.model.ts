export class Respuesta {
  constructor(
    public respuesta?: any,
    public pregunta?: any,
    public cliente?: any,
    public fecha_R?: Date,
    public Encuestaid?: number,
    public id_Respuesta?: number
  ) { }
}

export class Encuestas {
  constructor(
    public codigo?: any,
    public cliente?: any,
    public para?: any,
    public email?: any,
    public servicio?: any,
    public fecha_E?: any,
    public fecha_R?: any
  ) { }
}

export class Resultados {
  constructor(
    public pregunta?: any,
    public respuesta?: any
  ) { }
}
