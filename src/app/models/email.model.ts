export class Email {
  constructor(
    public asunto: string,
    public email: string,
    public cliente: string,
    public servicio: string,
    public para: string,
  ) { }
}
