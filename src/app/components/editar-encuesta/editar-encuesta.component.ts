import { Component, OnInit } from '@angular/core';
import { EncuestaService } from '../../services/encuesta.service';
import { NgForm } from '@angular/forms';
import { Encuesta } from 'src/app/models/encuesta.model';
import { Router, ActivatedRoute } from '@angular/router';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-editar-encuesta',
  templateUrl: './editar-encuesta.component.html',
  styleUrls: ['./editar-encuesta.component.css']
})
export class EditarEncuestaComponent implements OnInit {

  encuesta: Encuesta = new Encuesta('');
  constructor(public encuestaSErvice: EncuestaService, public router: Router,
    public activatedRoute: ActivatedRoute) {

    activatedRoute.params.subscribe(params => {
      const id_Encuesta = params['id_Encuesta'];


      this.cargarEncuesta(id_Encuesta);

    });
  }

  ngOnInit() {
  }

  cargarEncuesta(id: number) {
    this.encuestaSErvice.cargarEncuesta(id).subscribe(encuesta => {
      this.encuesta = encuesta;
      console.log('Encuesta cargada', this.encuesta);
    });
  }


  actualizarEncuesta(f: NgForm) {
    console.log(f.valid);
    console.log(f.value);

    if (!f.valid) {
      return;
    }
    this.encuestaSErvice.editarEncuesta(this.encuesta).subscribe(encuesta => {
      this.encuesta.id_Encuesta = encuesta.id_Encuesta;
      this.router.navigate(['/actualizar', encuesta.id_Encuesta]);
    });
  }

  public captureScreen() {
    var data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      var imgWidth = 208;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataUrl = canvas.toDataURL('image/png');
      let pdf = new jspdf('p', 'mm', 'a4');
      var position = 0;
      pdf.addImage(contentDataUrl, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('mypdf');
    });
  }
}
