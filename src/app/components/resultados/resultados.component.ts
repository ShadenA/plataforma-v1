import { Respuesta } from './../../models/respuesta.model';
import { Component, OnInit } from '@angular/core';
import { Encuesta } from '../../models/encuesta.model';
import { EncuestaService } from '../../services/encuesta.service';
import { ActivatedRoute } from '@angular/router';
import { RespuestaService } from '../../services/respuesta.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Encuestas, Resultados } from '../../models/respuesta.model';

@Component({
  selector: 'app-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.css']
})
export class ResultadosComponent implements OnInit {

  encuesta: Encuesta[] = [];
  Encuestaid: number;
  respuestas: Respuesta[] = [];
  respuesta: Respuesta[] = [];
  cargando = true;
  cargando2 = true;
  forma: FormGroup;
  cliente: null;
  encuestas: Encuestas[] = [];
  form: FormGroup;
  servicio: null;
  codigo: null;
  codigos: Resultados[] = [];
  persona = null;



  constructor(public encuestaService: EncuestaService, public activatedRoute: ActivatedRoute,
    public respuestaService: RespuestaService) {
    activatedRoute.params.subscribe(params => {
      const id_Encuesta = params['id_Encuesta'];
      this.cargarEncuesta(id_Encuesta);
    });
  }

  ngOnInit() {
    this.forma = new FormGroup({
      cliente: new FormControl(null, Validators.required),
      servicio: new FormControl(null, Validators.required)
    });
    // this.form = new FormGroup({
    //   codigo: new FormControl(null)
    // });
  }


  cargarEncuesta(id: number) {
    this.encuestaService.cargarEncuesta(id).subscribe(encuesta => {
      this.encuesta = encuesta;
      this.Encuestaid = encuesta.id_Encuesta;
      // this.cargarRespuesta(this.Encuestaid);
    });
  }

  resultados(codigo: number, para: string) {
    this.respuestaService.cargarResultados(codigo).subscribe((resp: any) => {
      this.codigos = resp.respuestas;
      this.persona = para;
      console.log(this.codigos);
    });
  }

  // cargarRespuesta(Encuestaid: number) {
  //   this.respuestaService.cargarRespuesta(Encuestaid).subscribe((resp: any) => {
  //     this.respuestas = resp.respuesta;
  //     console.log('Cleintes cargados', this.respuestas);
  //     this.cargando = false;
  //   });
  // }

  // cargarRespuestas() {
  //   let cliente = this.forma.value.cliente;
  //   this.respuestaService.cargarRespuestas(cliente).subscribe((resp: any) => {
  //     this.encuestas = resp.respuestas
  //     console.log('Encuestas cliente', this.encuestas);
  //     this.cliente = cliente;
  //     console.log('Cliente seleccionado', this.cliente);
  //     this.cargando2 = false;
  //   });
  // }

  cargarEnviados() {
    const cliente = this.forma.value.cliente;
    const servicio = this.forma.value.servicio;
    if (cliente !== '' && servicio === null) {
      this.respuestaService.consultarClientes(cliente).subscribe((resp: any) => {
        this.encuestas = resp.encuestas;
        console.log('Resultados cargados', this.encuestas);
      });
    } else if (servicio !== '' && cliente === null) {
      this.respuestaService.consultarServicios(servicio).subscribe((resp: any) => {
        this.encuestas = resp.encuestas;
        console.log('Resultados cargados', this.encuestas);
      });
    } else if (cliente !== '' && servicio !== '') {
      this.respuestaService.cargarEnviados(cliente, servicio).subscribe((resp: any) => {
        this.encuestas = resp.encuestas;
        console.log('Resultados cargados', this.encuestas);
      });
    }
  }

  // cargarRespuest() {
  //   let codigo = this.form.value.codigo;
  //   this.respuestaService.cargarRespuest(codigo).subscribe((resp: any) => {
  //     this.codigos = resp.respuestas;
  //     console.log('Respuestas del codigo', this.codigos);
  //   });
  // }
}
