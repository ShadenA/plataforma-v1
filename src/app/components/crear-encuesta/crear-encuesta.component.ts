
import { Component, OnInit } from '@angular/core';

import { Encuesta } from 'src/app/models/encuesta.model';
import { EncuestaService } from '../../services/encuesta.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-crear-encuesta',
  templateUrl: './crear-encuesta.component.html',
  styleUrls: ['./crear-encuesta.component.css']
})
export class CrearEncuestaComponent implements OnInit {

  forma: FormGroup;



  pregunta = false;

  constructor(public encuestaService: EncuestaService) {
  }

  ngOnInit() {
    this.forma = new FormGroup({
      titulo: new FormControl(null, Validators.required),
    });
  }

  crearP() {
    this.pregunta = !this.pregunta;
  }


  guardarEncuesta() {
    if (this.forma.invalid) {
      return;
    }

    const encuesta = new Encuesta(
      this.forma.value.titulo
    );

    this.encuestaService.crearEncuesta(encuesta).subscribe();
  }



}
