import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm, FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { Encuesta } from '../../models/encuesta.model';
import { EncuestaService } from '../../services/encuesta.service';
import Swal from 'sweetalert2';
import { UsuarioService } from '../../services/usuario.service';
import { Email } from '../../models/email.model';
import { Usuario } from 'src/app/models/usuario.models';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  encuestas: Encuesta[] = [];
  fecha: any;
  cargando = true;
  hasta: Date;
  ver: false;
  usuario: Usuario;
  correo = false;
  forma: FormGroup;
  url: string = null;
  datos: any[] = [null];
  cont = 0;
  cant = 0;

  // @ViewChild('content') content: ElementRef;

  constructor(public encuestaService: EncuestaService, public usuarioService: UsuarioService, private formBuilder: FormBuilder) {
    this.hasta = new Date();
    console.log(this.hasta);
  }

  ngOnInit() {
    this.usuario = this.usuarioService.usuario;
    this.cargarEncuestas();
    this.forma = this.formBuilder.group({});
    this.forma.addControl(`asunto`, new FormControl(null, Validators.required));
    this.forma.addControl(`cliente`, new FormControl(null));
    this.forma.addControl(`servicio`, new FormControl(null));
    this.forma.addControl(`para0`, new FormControl(null, Validators.required));
    this.forma.addControl(`email0`, new FormControl(null, Validators.required));
  }
  cargarEncuestas() {
    this.encuestaService.getEncuestas().subscribe((resp: any) => {
      this.encuestas = resp.encuestas;
      console.log('Encuestas cargadas', this.encuestas);
      this.cargando = false;
    });
  }

  borrarEncuesta(encuesta: Encuesta) {

    Swal.fire({
      title: '¿Esta seguro?',
      text: 'Esta a punto de borrar la encuesta: ' + encuesta.titulo,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Borrar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.value) {
        this.encuestaService.borrarEncuesta(encuesta.id_Encuesta).subscribe(borrado => {
          console.log(borrado);
          this.cargarEncuestas();
        });
      }
    });
  }

  verEncuesta(id: number) {

    this.encuestaService.cargarEncuesta(id).subscribe(resp => {
      console.log(resp);
    });

  }

  enviarCorreo() {
    this.cant = this.datos.length;
    for (let index = 0; index < this.datos.length; index++) {
      this.datos[index] = { para: this.forma.get(`para${index}`).value, email: this.forma.get(`email${index}`).value };
      console.log(this.datos[index].email);
      const email: Email = new Email(this.forma.value.asunto, this.datos[index].email, this.forma.value.cliente,
        this.forma.value.servicio, this.datos[index].para);
      this.usuarioService.enviarCorreo(email, index, this.cant).subscribe();
    }
    console.log(this.url);
  }

  add() {
    this.cont += 1;
    this.forma.addControl(`para${this.cont}`, new FormControl(null, Validators.required));
    this.forma.addControl(`email${this.cont}`, new FormControl(null, Validators.required));
    this.datos.push(null);
    console.log(this.forma.get(`email0`).value);
  }
  remove() {
    this.datos.splice(this.cont, 1);
    this.cont -= 1;
  }
}
