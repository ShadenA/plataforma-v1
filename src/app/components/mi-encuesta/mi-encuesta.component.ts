import { Component, OnInit } from '@angular/core';

import { EncuestaService } from '../../services/encuesta.service';
import { ActivatedRoute } from '@angular/router';
import { Encuesta } from 'src/app/models/encuesta.model';
import { PreguntaService } from '../../services/pregunta.service';
import { Pregunta } from 'src/app/models/pregunta.model';
import { OpcionService } from '../../services/opcion.service';
import { Opcion } from 'src/app/models/opcion.model';


@Component({
  selector: 'app-mi-encuesta',
  templateUrl: './mi-encuesta.component.html',
  styleUrls: ['./mi-encuesta.component.css']
})
export class MiEncuestaComponent {

  encuestas: Encuesta;
  preguntas: Pregunta[] = [];
  cargando = true;
  titulo: string;
  Encuestaid: number;
  opciones: Opcion[] = [];
  otros = false;
  id: any;

  constructor(public encuestaService: EncuestaService, public activatedRoute: ActivatedRoute,
    public preguntaService: PreguntaService, public opcionService: OpcionService) {
    activatedRoute.params.subscribe(params => {
      const id_Encuesta = params['id_Encuesta'];
      this.cargarEncuesta(id_Encuesta);
      localStorage.setItem('id', JSON.stringify(id_Encuesta));
    });
  }

  ngOnInit() {
  }

  cargarEncuesta(id: number) {
    this.encuestaService.cargarEncuesta(id).subscribe(encuestas => {
      this.encuestas = encuestas;
      this.titulo = encuestas.titulo;
      this.Encuestaid = encuestas.id_Encuesta;
      console.log('Encuesta cargada', this.encuestas);
      this.cargarPreguntas(this.Encuestaid);
    });
  }

  cargarPreguntas(Encuestaid: number) {
    this.preguntaService.cargarPreguntas(Encuestaid).subscribe((resp: any) => {
      this.preguntas = resp.preguntas;
      console.log('Preguntas cargadas', this.preguntas);
      this.cargarOpciones();
      this.cargando = false;
    });
  }

  cargarOpciones() {
    this.opcionService.cargarOpciones().subscribe((resp: any) => {
      this.opciones = resp.opciones;
      console.log('Opciones cargadas', this.opciones);
    });
  }


  mostrarOtros() {
    this.otros = !this.otros;
  }



}
