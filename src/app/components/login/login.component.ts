
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


import { Usuario } from '../../models/usuario.models';
import { UsuarioService } from '../../services/usuario.service';
import { Router } from '@angular/router';
import { resolve } from 'url';

declare const gapi: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  styles: [
    `
      .ng-invalid.ng-touched:not(form) {
        border: 1px solid red;
      }
    `
  ]
})
export class LoginComponent implements OnInit {

  correo: string;

  recuerdame = false;

  auth2: any;

  constructor(
    public usuarioService: UsuarioService,
    private router: Router) { }


  ngOnInit() {
    this.googleInit();
    this.correo = localStorage.getItem('correo') || '';
    if (this.correo.length > 1) {
      this.recuerdame = true;
    }

  }

  googleInit() {
    gapi.load('auth2', () => {
      this.auth2 = gapi.auth2.init({
        client_id: '965925012407-5bu5ub1ho1q7rnp53ltfatv49lhke4eh.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        scope: 'profile email'
      });
      this.attachSignin(document.getElementById('btnGoogle'));
    });
  }

  attachSignin(element) {
    this.auth2.attachClickHandler(element, {}, (googleUser) => {
      // let profile = googleUser.getBasicProfile();
      let token = googleUser.getAuthResponse().id_token;
      this.usuarioService.loginGoogle(token)
        .subscribe(resp => {
          window.location.href = '#/inicio';
          console.log(resp);
        });
    });
  }


  ingresar(forma: NgForm) {

    if (forma.invalid) {
      return;
    }

    const usuario = new Usuario(null, forma.value.correo, forma.value.password)
    this.usuarioService.login(usuario, forma.value.recuerdame)
      .subscribe((resp: any) => {
        this.router.navigate(['/inicio']);
        console.log(resp);
      });
  }
}
