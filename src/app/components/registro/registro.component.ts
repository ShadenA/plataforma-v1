import { Component, OnInit } from "@angular/core";
import { NgForm, FormControl, Validators } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { UsuarioService } from '../../services/usuario.service';
import { Usuario } from '../../models/usuario.models';
import { Router } from '@angular/router';




@Component({
  selector: "app-registro",
  templateUrl: "./registro.component.html",
  styleUrls: ["./registro.component.css"]
})
export class RegistroComponent implements OnInit {
  forma: FormGroup;
  constructor(public usuarioService: UsuarioService,
    public router: Router) { }

  usuario = {
    nombre: null,
    correo: null,
    password: null
  };

  ngOnInit() {
    this.forma = new FormGroup({
      nombre: new FormControl(null, Validators.required),
      correo: new FormControl(null, [Validators.required, Validators.email, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]),
      password: new FormControl(null, [Validators.required, Validators.minLength(8)])
    });
  }

  registrarUsuario() {

    const usuario = new Usuario(
      this.forma.value.nombre,
      this.forma.value.correo,
      this.forma.value.password
    );

    this.usuarioService.crearUsuario(usuario)
      .subscribe(resp => this.router.navigate(['/login']));
  }

}
