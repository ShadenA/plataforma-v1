import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NoPagedFoundComponent } from './shared/no-paged-found/no-paged-found.component';
import { CrearEncuestaComponent } from './components/crear-encuesta/crear-encuesta.component';
import { EditarEncuestaComponent } from './components/editar-encuesta/editar-encuesta.component';
import { MiEncuestaComponent } from './components/mi-encuesta/mi-encuesta.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { RegistroComponent } from './components/registro/registro.component';
import { LoginGuardGuard } from './services/guards/login-guard.guard';
import { ResultadosComponent } from './components/resultados/resultados.component';

const RUTAS: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registro', component: RegistroComponent },
  { path: 'inicio', component: InicioComponent, canActivate: [LoginGuardGuard] },
  { path: 'crear', component: CrearEncuestaComponent, canActivate: [LoginGuardGuard] },
  { path: 'actualizar/:id_Encuesta', component: EditarEncuestaComponent, canActivate: [LoginGuardGuard] },
  { path: 'encuesta/:id_Encuesta', component: MiEncuestaComponent, canActivate: [LoginGuardGuard] },
  { path: 'resultado/:id_Encuesta', component: ResultadosComponent, canActivate: [LoginGuardGuard] },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: NoPagedFoundComponent }
];

export const APP_ROUTES = RouterModule.forRoot(RUTAS, { useHash: true });
