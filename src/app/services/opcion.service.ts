import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class OpcionService {

  constructor(public http: HttpClient) { }


  cargarOpciones() {
    const url = URL_SERVICIOS + '/opciones'
    return this.http.get(url);
  }
}
