import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from '../config/config';




@Injectable({
  providedIn: 'root'
})
export class PreguntaService {




  constructor(public http: HttpClient) { }

  cargarPreguntas(Encuestaid: number) {
    const url = URL_SERVICIOS + '/preguntas/' + Encuestaid;
    return this.http.get(url);
  }



}
