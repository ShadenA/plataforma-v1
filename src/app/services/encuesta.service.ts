import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Encuesta } from '../models/encuesta.model';
import { URL_SERVICIOS } from '../config/config';
import { map } from 'rxjs/operators';


import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root'
})
export class EncuestaService {


  constructor(public http: HttpClient) {
    console.log('Servicio listo para usarse');
  }

  getEncuestas() {
    const url = URL_SERVICIOS + '/encuestas';
    return this.http.get(url);
  }

  borrarEncuesta(id_Encuesta: number) {
    const url = URL_SERVICIOS + '/encuestas/' + id_Encuesta;
    return this.http.delete(url).pipe(map(reso => {
      Swal.fire('Encuesta borrada', 'La encuesta ha sido eliminada correctamente', 'success');
      return true;
    }));
  }

  cargarEncuesta(id: number) {
    const url = URL_SERVICIOS + '/encuestas/' + id;
    return this.http.get(url).pipe(map((resp: any) => resp.encuesta));
  }

  editarEncuesta(encuesta: Encuesta) {
    const url = URL_SERVICIOS + '/encuestas/' + encuesta.id_Encuesta;
    return this.http.put(url, encuesta).pipe(map((resp: any) => {
      Swal.fire('Encuesta actualizada', encuesta.titulo, 'success');
      return resp.encuesta;
    }));
  }

  crearEncuesta(encuesta: Encuesta) {
    const url = URL_SERVICIOS + '/encuestas/crear';
    return this.http.post(url, encuesta).pipe(map((resp: any) => {
      return resp.encuesta;
    }));
  }



}
