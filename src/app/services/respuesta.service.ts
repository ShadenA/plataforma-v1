import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Respuesta } from '../models/respuesta.model';
import { URL_SERVICIOS } from '../config/config';
import { map, catchError } from 'rxjs/operators';
import { empty } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class RespuestaService {

  constructor(public http: HttpClient) {
  }

  guardarRespuesta(Encuestaid: number, respuesta: Respuesta) {
    const url = URL_SERVICIOS + '/respuesta/' + Encuestaid;

    return this.http.post(url, respuesta);
  }

  cargarEnviados(cliente: string, servicio: string) {
    const url = URL_SERVICIOS + '/respuesta/' + cliente + '/' + servicio;
    return this.http.get(url).pipe(map(resp => {
      return resp;
    }), catchError(err => {
      Swal.fire('Advertencia', 'No hay resultados para mostrar, por favor elija otra opción', 'warning');
      return err;
    }));

  }
  consultarServicios(servicio: string) {
    const url3 = URL_SERVICIOS + '/buscar/' + servicio;
    return this.http.get(url3).pipe(map(resp => {
      return resp;
    }), catchError(err => {
      Swal.fire('Advertencia', 'No hay resultados para mostrar, por favor elija otra opción', 'warning');
      return err;
    }));

  }

  consultarClientes(cliente: string) {
    const url2 = URL_SERVICIOS + '/busqueda/' + cliente;
    return this.http.get(url2).pipe(map(resp => {
      return resp;
    }), catchError(err => {
      Swal.fire('Advertencia', 'No hay resultados para mostrar, por favor elija otra opción', 'warning');
      return err;
    }));

  }


  cargarResultados(codigo: number) {

    const url = URL_SERVICIOS + '/respuest/' + codigo;
    return this.http.get(url);
  }


  // cargarRespuesta(Encuestaid: number) {
  //   const url = URL_SERVICIOS + '/respuestas/' + Encuestaid;
  //   return this.http.get(url);
  // }

  // cargarRespuestas(cliente: string) {
  //   const url = URL_SERVICIOS + '/respuesta/' + cliente;
  //   return this.http.get(url);
  // }

  // cargarRespuest(codigo: number) {
  //   const url = URL_SERVICIOS + '/respuest/' + codigo;
  //   return this.http.get(url);
  // }
}
