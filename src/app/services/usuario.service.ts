import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.models';
import { URL_SERVICIOS } from '../config/config';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';


import Swal from 'sweetalert2';
import { Observable, empty } from 'rxjs';
import { Router } from '@angular/router';
import { Email } from '../models/email.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuario: Usuario;
  token: string;

  constructor(public http: HttpClient, public router: Router) {
    this.cargarStorage();
    console.log('Servicio de usuario listo');
  }

  estaLogueado() {
    return (this.token.length > 5) ? true : false;
  }
  cargarStorage() {
    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    } else {
      this.token = '';
      this.usuario = null;
    }
  }



  guardarStorage(token: string, usuario: Usuario) {

    localStorage.setItem('token', token);
    localStorage.setItem('usuario', JSON.stringify(usuario));
    this.usuario = usuario;
    this.token = token;

  }

  logout(usuario: Usuario) {



    const url = URL_SERVICIOS + '/conexion';

    return this.http.put(url, usuario)
      .pipe(map(resp => {
        console.log(resp);
        this.usuario = null;
        this.token = '';

        localStorage.removeItem('token');
        localStorage.removeItem('usuario');
        localStorage.removeItem('id');
        localStorage.removeItem('json');
        this.router.navigate(['/login']);
        return true;
      }));
  }

  loginGoogle(token: string) {

    const url = URL_SERVICIOS + '/login/google';

    return this.http.post(url, { token })
      .pipe(map((resp: any) => {
        this.guardarStorage(resp.token, resp.usuario);
        console.log(resp);
        return true;
      }));
  }

  login(usuario: Usuario, recordar = false) {

    if (recordar) {

      localStorage.setItem('correo', usuario.correo);

    } else {
      localStorage.removeItem('correo');
    }

    const url = URL_SERVICIOS + '/login';
    return this.http.post(url, usuario)
      .pipe(map((resp: any) => {
        this.guardarStorage(resp.token, resp.usuario);
        console.log(resp);
        return true;
      }), catchError((err: any) => {
        this.router.navigate(['/login']);
        console.log(err);
        Swal.fire({
          title: 'Error en el login',
          text: 'Correo o contraseña inválidos',
          icon: 'error'
        });
        return empty();
      }));


  }

  crearUsuario(usuario: Usuario) {

    const url = URL_SERVICIOS + '/registro';
    return this.http.post(url, usuario)
      .pipe(map((resp: any) => {
        Swal.fire({
          title: 'Usuario creado',
          text: usuario.correo,
          icon: 'success'
        });
        return resp.usuario;
      }), catchError((err: any) => {
        Swal.fire({
          title: 'Error en el registro',
          text: 'El correo ya existe!!! ',
          icon: 'error'
        });
        return empty();
      }));

  }

  enviarCorreo(email: Email, index, cant) {
    const url = URL_SERVICIOS + '/email';
    return this.http.post(url, email)
      .pipe(map((resp: any) => {
        if ((index + 1) === cant) {
          Swal.fire({
            title: 'Envio exitoso',
            text: 'El correo se ha enviado exitosamente',
            icon: 'success'
          });
          console.log(resp);
          return resp;
        }
      }), catchError((err: any) => {
        Swal.fire({
          title: 'Envio fallido',
          text: 'Credenciales de usuario incorrectas',
          icon: 'error'
        });
        return empty();
      }));
  }


}
