import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-paged-found',
  templateUrl: './no-paged-found.component.html',
  styleUrls: ['./no-paged-found.component.css']
})
export class NoPagedFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
