import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { APP_ROUTES } from './app.routes';

import { AppComponent } from './app.component';

import { LoginComponent } from './components/login/login.component';
import { CrearEncuestaComponent } from './components/crear-encuesta/crear-encuesta.component';
import { EditarEncuestaComponent } from './components/editar-encuesta/editar-encuesta.component';
import { MiEncuestaComponent } from './components/mi-encuesta/mi-encuesta.component';
import { NoPagedFoundComponent } from './shared/no-paged-found/no-paged-found.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistroComponent } from './components/registro/registro.component';
import { HttpClientModule } from '@angular/common/http';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { LoginGuardGuard } from './services/guards/login-guard.guard';
import { ResultadosComponent } from './components/resultados/resultados.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CrearEncuestaComponent,
    EditarEncuestaComponent,
    MiEncuestaComponent,
    NoPagedFoundComponent,
    NavbarComponent,
    InicioComponent,
    RegistroComponent,
    SidebarComponent,
    ResultadosComponent
  ],

  imports: [BrowserModule, FormsModule, ReactiveFormsModule, HttpClientModule, APP_ROUTES],
  providers: [LoginGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
